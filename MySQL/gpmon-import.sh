#!/bin/sh

set -ex
echo "Import database dump from NERSC"
wget -O /mysql/gpstatdb-dump.sql.gz http://portal.nersc.gov/project/genepool/gpstatdb-dump.sql.gz
gzip -dv /mysql/gpstatdb-dump.sql.gz
chmod 644 /mysql/gpstatdb-dump.sql

echo "Create database gpmondb;" | mysql -u root -p$MYSQL_ROOT_PASSWORD
cat /mysql/gpstatdb-dump.sql    | mysql -u root -p$MYSQL_ROOT_PASSWORD gpmondb
