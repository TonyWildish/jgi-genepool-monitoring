#!/bin/sh

set -e
/mysql/docker-entrypoint.sh mysqld 2>&1 | tee /log/docker-entrypoint.log >/dev/null
