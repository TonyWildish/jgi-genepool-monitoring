# Build/push the mysql container:
docker build -f Dockerfile.gpmon-mysqld -t tonywildish/gpmon-mysqld .
docker push tonywildish/gpmon-mysqld

# Run the mysql container:
docker run -e MYSQL_ROOT_PASSWORD=asdf --rm \
  --volume `pwd`/tmp:/tmp \
  --name gpmon-mysqld \
  --detach \
  tonywildish/gpmon-mysqld

#
# Run another mysql container to check the networking is OK
docker run --rm -it \
  --entrypoint /bin/bash \
  --name gpmon-sqltest \
  --link gpmon-mysqld:gpmon-sqltest \
  tonywildish/gpmon-mysqld
#
# Once in the container, run 'mysql -h gpmon-mysqld -u root -pasdf gpmondb' and try 'select * from queues;''

#
# Build/push the shiny container:
docker build -t tonywildish/gpmon-shiny -f Dockerfile.gpmon-shiny .
docker push tonywildish/gpmon-shiny

#
# Run the shiny app!
docker run --rm \
  --name gpmon-shiny \
  --link gpmon-mysqld:gpmon-shiny \
  --detach \
  tonywildish/gpmon-shiny

#
# Log into the shiny app and check DB connectivity
docker exec -it \
  gpmon-shiny \
  /bin/bash
#
# and try the mysql command, above...

#
# Dump the gpstatdb contents for my demo
mysqldump -h staffdb01.nersc.gov -u tonywildish -p \
  --set-gtid-purged=OFF \
  --single-transaction \
  gpstatdb queues queue_history | tee gpstatdb-dump.sql
